# Glomo Test for Junior Fullstack developer

Small app created for GLOMO testing purpose, built with React and Node.js.

This app fills the database with data (sporting events) from the JSON file and then enables voting on them. 

The app consists of two pages, Home page that will list sporting events, and Reset/Insert page that can be used for inserting/resetting the database.

## Installation

Clone the project from bitbucket

and run npm install
```bash
npm install
```


## Usage

Run 
```python
npm start
```

On the home page, there will be a list of all events that the user can vote on. On the first load, the database will be empty so you will need to insert data first.

To achieve this, go on the Reset/Insert page and click the insert button. This action will load data from the JSON file and populate the database. This page also contains the reset button, which can be used for removing added events.
After inserting the data, you can visit the Home page and vote on the events. 

Events will be loaded randomly, so on each page load, you can vote on different sport matches.
Happy voting!

## Technology used

### Frontend
React (built with create-react-app)

##### React libraries used:
- node-sass
- react, react-dom, react-router, react-router-dom, react scripts
- superagent, superagent-promise

### Server
NodeJS

##### NodeJS libraries used:
- express
- mongodb
- mongoose
- nodemon
- request
- mocha
- chai

### Database
MongoDB hosted on Atlas Cloud


## Development
Time used: 10h

Around 3h spend on design (I prefer getting a design ready and then working on it, otherwise, I can get stuck on it). Due to the time limit, the voting part was left unfinished (not styled properly).

The biggest challenge was to create a voting API and to register an update to React Component.  One of the future improvements would include adding a session that will register only one vote per session with the option of re-voting if desired.

I've added only one test. Reason for that is that I haven't done much of testing in work I've done so far, and this is my first time using the Mocha library.



