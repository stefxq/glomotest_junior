import React, { Component } from "react";
import { BrowserRouter, Switch } from 'react-router-dom';
import AppRoutes from './routes'

import './Styles/sass/misc/global.scss'

class App extends Component {
  state = {

  };
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <AppRoutes />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;