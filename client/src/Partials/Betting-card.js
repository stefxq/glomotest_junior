import React from 'react'
import '../Styles/sass/partials/bettingcard.scss'

const convertVote = (e) => {
    switch (e) {
        case '1':
            return 'homeWin'
        case 'X':
            return 'draw'
        case '2':
            return 'awayWin'
        default:
            return 'homeWin'
    }
}

const handleClick = (e, props) => {
    let dataToPass = props.data

    const obj = {
        clicked: convertVote(e.target.innerText)
    }
    dataToPass = { ...dataToPass, ...obj }
    props.handleVote(dataToPass)
}

const BettingCard = (props) => {
    const { name, country, state, homeWin, draw, awayWin } = props.data
    return (
        <div className="column">
            <div className="bet-info">
                <h3 className="bet-name">{name}</h3>
                <div className="country">{country}</div>
                <div className={state.toLowerCase() + " status"}><span>{state}</span></div>
            </div>

            <div className="options">
                <span className="home-win" onClick={(e) => handleClick(e, props)}>
                    1
                </span>

                <span className="draw" onClick={(e) => handleClick(e, props)}>
                    X
                </span>

                <span className="away-win" onClick={(e) => handleClick(e, props)}>
                    2
                </span>

            </div>
            <div className='counter'>
                <div>{homeWin}</div>
                <div>{draw}</div>
                <div>{awayWin}</div>
            </div>
        </div>
    )
};
BettingCard.defaultProps = {
    data: [{
        name: 'Liverpool - Arsenal',
        country: 'England',
        state: 'Started',
    }],
    message:'Default message'
}

export default BettingCard