import React from 'react';
import { Link, Route } from 'react-router-dom';

const GlomoLink = ({ label, to, activeOnlyWhenExact }) => (
    <Route
        path={to}
        exact={activeOnlyWhenExact}
        children={({ match }) => (
                <Link to={to} href={to}>
                    {label}
                </Link>
        )}
    />
    );

export default GlomoLink;
