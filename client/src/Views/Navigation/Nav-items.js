import React, { Component } from "react";
import { Link, Route } from 'react-router-dom';
import "../../Styles/sass/Navigation/navigation.scss";



const MenuLink = ({ label, to, activeOnlyWhenExact }) => (
    <Route
        path={to}
        exact={activeOnlyWhenExact}
        children={({ match }) => (
            <li>
                <Link to={to} href={to}>
                    {label}
                </Link>
            </li>
        )}
    />
);

class NavItems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: window.innerWidth,
            mobileToggle: false
        };
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);        
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };

    handleMobileClick = () => {
        this.setState({
            mobileToggle: !this.state.mobileToggle
        })
    }

    

    renderDesktop = () => {
        return (
            <nav className="main-navigation">
               <div className="nav-left"></div>
                <div className="nav-center">
                    <div className="left-side">
                        
                    </div>
                    <div className="rigth-side">
                        
                    </div>
                </div>
                <div className="nav-right">
                    <ul>
                        <MenuLink key={'/'} to={'/'} label={'Home'} />
                        <MenuLink key={'/reset'} to={'/reset'} label={'Reset/Insert'} />
                        
                    </ul>
                
                </div>
            </nav>
        );
    }

    renderMobile = () => {
        return (
            <nav className={this.state.mobileToggle ? 'main-navigation active': 'main-navigation'} onClick={ this.handleMobileClick }>
                <div className='mobile--burger'>
                    <div className="line line_1"></div>
                    <div className="line line_2"></div>
                    <div className="line line_3"></div>
                </div>
                <div className='mobile-navigation'>
                    <ul onClick={ this.handleMobileClick }>
                        <MenuLink key={'/'} to={'/'} label={'Home'} />
                        <MenuLink key={'/reset'} to={'/reset'} label={'Reset/Insert'} />

                    </ul>
                </div>

            </nav>
        );
    }

    renderContent() {
        const { width } = this.state;
        const isMobile = width <= 768;
        if (isMobile) {
            return this.renderMobile()
        } else {
            return this.renderDesktop()
        }
    }
    render() {
        return (
            <header className="header"> {this.renderContent()} </header>
        );
    }
}

export default NavItems;

