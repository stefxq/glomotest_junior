import React, { Component } from "react";
import NavItems from "../Navigation/Nav-items";
import "../../Styles/sass/Navigation/navigation.scss";






class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scrolled: false
          };
    }
    
    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll)
    }

    

    handleScroll = () => {
        var nav = document.querySelector('.navigation');

        var bounding = nav.getBoundingClientRect();

        if (bounding.top!==0) {
            this.setState ({
                scrolled: true
            })
        } else {
            this.setState ({
                scrolled: false
            })
        }
    }

    renderContent(){
        return (
            <NavItems />
        )
    }


    render() {
        let className = 'navigation';
        if (this.state.scrolled) {
            className += ' scroll';
        }
        return (
            <div className={ className }>
                { this.renderContent() }
            </div>
        )
    }
}
export default Navigation;
  
