import React, { Component } from "react"
import { Bets } from '../../agent'
import BettingCard from '../../Partials/Betting-card'

class Frontpage extends Component {
    state = {
        data: [],
        message:'No data, please insert it on Reset/Insert page',
    }
    componentDidMount = () => {
        this.getRandomSport()
    }
    getRandomSport = () => {
        Bets.getRandom()
            .then((response) => {
                this.setState({ data: response })
                if (response.message) {
                    this.setState({ message: response.message})
                }
            })
    }

    getSportByCat = (sport) => {
        Bets.getSportByName(sport)
            .then((response) => {
                this.setState({ data: response })
                if (!Object.keys(response).length) {
                    const categoryToRemove = {
                        category: sport
                    }
                    Bets.removeCategory(categoryToRemove)
                    .then(response => {
                        this.setState({ message: 'You voted on all events in '+sport + '. Loading new events...'})
                        setTimeout( () => { this.getRandomSport() }, 1500);
                    })
                }
            })
    }

    handleVote = (element) => {
        const { data } = this.state
        Bets.vote(element)
            .then(() => {
                this.getSportByCat(data[0].sport)
            })
            .catch(err => {
                console.log(err)
            })
    }


    renderBettingCards = () => {
        const { data } = this.state
        return (
            data.map((element, key) => {
                return (
                    <BettingCard key={key} data={element} handleVote={this.handleVote} />
                )
            })
        )
    }

    render() {
        const { data, message } = this.state
        return (
            <div className='frontpage'>
                <div className='homepage-wrapper'>
                    <div className="wrapper-info">
                        <h1>Bet today!</h1>
                        <h2 className="subheader">{data.length > 0 ? data[0].sport : ''}</h2>
                    </div>
                </div>
                <div className="betting-section">
                    <div className="row">
                        {data.length > 0 ? this.renderBettingCards() : <span> { message } </span> }
                    </div>
                </div>
                <div className="response-message">

                </div>
            </div>
        );
    }
}

export default Frontpage;