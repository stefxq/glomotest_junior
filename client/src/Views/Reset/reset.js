import React, { Component } from "react"
import { Bets } from '../../agent'

class reset extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: ''
        };
    }
    reset = () => {
        Bets.resetDB()
            .then(() => {
                this.setState({ message: 'DB successfully reseted' })
            }).catch((err) => {
                console.log(err)
            });
    }

    insert = () => {
        Bets.insertIntoDB()
            .then((response) => {
                this.setState({ message: response })
            }).catch((err) => {
                console.log(err)
            });
    }


    render() {
        const { message } = this.state
        return (
            <div className='reset-page'>
                <div className='reset-section'>
                    <div className='reset-container'>
                        <button onClick={() => this.reset()}>Reset Events</button>
                        <button onClick={() => this.insert()}>Insert</button>
                        <div>
                            {message}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default reset;