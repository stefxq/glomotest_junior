import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import Navigation from './Views/Navigation/Navigation'
import Home from './Views/Home/Frontpage'
import Reset from './Views/Reset/reset'

const Routes = () => (
    <div id="main">
      <Navigation />
      <div className="container">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/reset" component={Reset} />

        </Switch>
      </div>
    </div>
  );
  
  export default withRouter(
    (Routes)
  );
