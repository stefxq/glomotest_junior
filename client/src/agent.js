import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';


const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = 'http://localhost:8000/__api/';
const responseBody = res => res.body;

const requests = {
  del: url =>
    superagent
      .del(`${API_ROOT}${url}`)
      .then(responseBody),
  get: url =>
    superagent
      .get(`${API_ROOT}${url}`)
      .set('Authorization')
      .accept('application/json')
      .then(responseBody),
  put: (url, body) =>
    superagent
      .put(`${API_ROOT}${url}`, body)
      .type('form')
      .then(responseBody),
  post: (url, body) =>
      superagent
        .post(`${API_ROOT}${url}`, body)
        .type('form')
        .then(responseBody),
};

export const Bets = {
  getAll: () => requests.get('allEvents'),
  getRandom: () => requests.get('randomSport'),
  getSportByName: sport => requests.get(`sportByName/${sport}`),
  insertIntoDB: () => requests.get('saveToDB'),

  vote: data => requests.put('voteOnGame', data),
  removeCategory: (sport) => requests.put('removeCategory', sport),

  resetDB: () => requests.del('clearDB'),
};


