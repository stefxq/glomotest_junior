const express = require('express')
const app = express()
const cors = require('cors')
const helmet = require('helmet')
const morgan = require('morgan')


const db_connection = require("./config/database-connection")
const insertRoute = require("./Routes/insertJSON")
const events = require("./Routes/events")



// adding Helmet to enhance your API's security
app.use(helmet());


app.use(express.urlencoded({extended:false}))

// enabling CORS for all requests
app.use(cors());

// use it before all route definitions

// adding morgan to log HTTP requests
app.use(morgan('combined'));





//app.use("/__api/",indexRoute)
app.use("/__api/",insertRoute)
app.use("/__api/",events)



app.get('*', function(req, res){
  res.send('Endpoint does not exists', 404);
});

app.listen(8000, () => {
    db_connection()
    
});