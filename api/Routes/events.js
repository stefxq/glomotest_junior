const express = require('express'),
    router = express.Router()


//Event model 
const Event = require('../Models/Events')
var categories = ['SNOOKER', 'FOOTBALL', 'TENNIS', 'ICE_HOCKEY', 'HANDBALL']
const state = ['STARTED', 'NOT_STARTED']


randomNum = (numOfCategories) => {
    return Math.floor(Math.random() * numOfCategories)
}

registerVoteToDB = (id, num, name) => {
    let returnMsg
    if (num === undefined) {
        var query = {}
        query[name] = 1
        query['voted'] = true
        Event.updateOne({ id: id }, query, (err) => { //if there are not votes, set them up
            if (err) throw err
            returnMsg = 'Vote successfuly registered'
        });
    } else {
        var query = {}
        query[name] = increaseCount(num)
        query['voted'] = true
        Event.updateOne({ id: id }, query, (err) => { //if there is, increase it
            if (err) throw err
            returnMsg = 'Vote successfuly registered'
        });
    }
    return returnMsg
}

increaseCount = (num) => {
    return parseInt(num, 10) + 1
}

router.put("/removeCategory", (req, res) => {
    const { category } = req.body
    var index = categories.indexOf(category)
    if (index !== -1) categories.splice(index, 1)
    res.status(200).send({ message: "Category all voted up!" })

})

router.get("/allEvents", (req, res) => {
    Event.find({}, (err, Events) => {
        if (err) throw err
        res.json(Events)
    });
})

router.get("/sportByName/:sport", (req, res) => {
    Event.find({ sport: req.params.sport, state: state, voted: false }, (err, Events) => {
        if (err) throw err
        res.json(Events)
    });
})

router.get("/randomSport", function (req, res) {
    Event.find({ sport: categories[randomNum(categories.length)], state: state, voted: false }, (err, Events) => {
        if (err) throw err
        if (Events.length) {
            res.json(Events)
        } else {
            res.json({ message: "You voted on everything! Good luck with your bets!" });
        }
    })
});


router.put("/voteOnGame", (req, res) => {
    const { id, clicked } = req.body

    Event.find({ id: id }, (err, Events) => {
        if (err) throw err
        const { homeWin, draw, awayWin } = Events[0]
        let numOfVotes
        if (clicked === 'homeWin') {
            numOfVotes = homeWin
        }
        if (clicked === 'draw') {
            numOfVotes = draw
        }
        if (clicked === 'awayWin') {
            numOfVotes = awayWin
        }
        res.json(registerVoteToDB(id, numOfVotes, clicked))
    });
});

router.delete("/clearDB", (req, res) => {
    Event.deleteMany({}, (err) => {
        if (err) throw err
        res.json('Successfully cleared table')
    });

    categories = []

    categories = ['SNOOKER', 'FOOTBALL', 'TENNIS', 'ICE_HOCKEY', 'HANDBALL']

    
    
})


module.exports = router