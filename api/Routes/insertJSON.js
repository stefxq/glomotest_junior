const express = require('express'),
    router = express.Router(),
    jsonFile = require('../config/loadJSON'),
    Vote = require('../Models/Events')

router.get('/saveToDB', async (req, res) => {
    jsonFile.forEach(element => {
        Vote.findOne({ id: element.id })
            .then(vote => {
                if (vote) {
                    return res.json('Event exists')
                } else {
                    const newVote = new Vote({
                        awayName: element.awayName,
                        createdAt: element.createdAt,
                        group: element.group,
                        homeName: element.homeName,
                        id: element.id,
                        name: element.name,
                        objectId: element.objectId,
                        sport: element.sport,
                        country: element.country,
                        state: element.state,
                        voted: false
                    })
                    newVote.save()
                }
                return res.json('Data successfully inserted')
            })
    });
})

module.exports = router
