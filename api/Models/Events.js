var mongoose = require("mongoose");
var voteSchema = new mongoose.Schema({
    awayName: String,
    createdAt: Date,
    group: String,
    homeName: String,
    id: Number,
    name: String,
    objectId: String,
    sport: String,
    country: String,
    state: String,
    homeWin: Number,
    draw: String,
    awayWin: Number,
    voted: Boolean,
});
module.exports = mongoose.model("Vote", voteSchema);