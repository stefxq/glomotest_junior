var expect  = require('chai').expect;
var request = require('request');

it('Missing endpoint', function(done) {
    request('http://localhost:8000' , (error, response, body) => {
        expect(body).to.equal('Endpoint does not exists');
        done();
    });
});

